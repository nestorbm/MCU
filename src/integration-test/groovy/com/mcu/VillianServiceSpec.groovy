package com.mcu

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class VillianServiceSpec extends Specification {

    VillianService villianService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Villian(...).save(flush: true, failOnError: true)
        //new Villian(...).save(flush: true, failOnError: true)
        //Villian villian = new Villian(...).save(flush: true, failOnError: true)
        //new Villian(...).save(flush: true, failOnError: true)
        //new Villian(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //villian.id
    }

    void "test get"() {
        setupData()

        expect:
        villianService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Villian> villianList = villianService.list(max: 2, offset: 2)

        then:
        villianList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        villianService.count() == 5
    }

    void "test delete"() {
        Long villianId = setupData()

        expect:
        villianService.count() == 5

        when:
        villianService.delete(villianId)
        sessionFactory.currentSession.flush()

        then:
        villianService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Villian villian = new Villian()
        villianService.save(villian)

        then:
        villian.id != null
    }
}

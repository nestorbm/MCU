package com.mcu

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class TypeCharacterServiceSpec extends Specification {

    TypeCharacterService typeCharacterService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new TypeCharacter(...).save(flush: true, failOnError: true)
        //new TypeCharacter(...).save(flush: true, failOnError: true)
        //TypeCharacter typeCharacter = new TypeCharacter(...).save(flush: true, failOnError: true)
        //new TypeCharacter(...).save(flush: true, failOnError: true)
        //new TypeCharacter(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //typeCharacter.id
    }

    void "test get"() {
        setupData()

        expect:
        typeCharacterService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<TypeCharacter> typeCharacterList = typeCharacterService.list(max: 2, offset: 2)

        then:
        typeCharacterList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        typeCharacterService.count() == 5
    }

    void "test delete"() {
        Long typeCharacterId = setupData()

        expect:
        typeCharacterService.count() == 5

        when:
        typeCharacterService.delete(typeCharacterId)
        sessionFactory.currentSession.flush()

        then:
        typeCharacterService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        TypeCharacter typeCharacter = new TypeCharacter()
        typeCharacterService.save(typeCharacter)

        then:
        typeCharacter.id != null
    }
}

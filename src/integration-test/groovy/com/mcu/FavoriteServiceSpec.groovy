package com.mcu

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class FavoriteServiceSpec extends Specification {

    FavoriteService favoriteService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Favorite(...).save(flush: true, failOnError: true)
        //new Favorite(...).save(flush: true, failOnError: true)
        //Favorite favorite = new Favorite(...).save(flush: true, failOnError: true)
        //new Favorite(...).save(flush: true, failOnError: true)
        //new Favorite(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //favorite.id
    }

    void "test get"() {
        setupData()

        expect:
        favoriteService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Favorite> favoriteList = favoriteService.list(max: 2, offset: 2)

        then:
        favoriteList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        favoriteService.count() == 5
    }

    void "test delete"() {
        Long favoriteId = setupData()

        expect:
        favoriteService.count() == 5

        when:
        favoriteService.delete(favoriteId)
        sessionFactory.currentSession.flush()

        then:
        favoriteService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Favorite favorite = new Favorite()
        favoriteService.save(favorite)

        then:
        favorite.id != null
    }
}

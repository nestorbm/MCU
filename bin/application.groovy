

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.mcu.Usuario'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.mcu.UsuarioRole'
grails.plugin.springsecurity.authority.className = 'com.mcu.Role'
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/redirect'
grails.plugin.springsecurity.securityConfigType = "InterceptUrlMap"
grails.plugin.springsecurity.interceptUrlMap  = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/adminHome.gsp',  access: ['ROLE_ADMIN']],
	[pattern: '/adminHome',      access: ['ROLE_ADMIN']],
	[pattern: '/redirect',       access: ['ROLE_ADMIN']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/usuario/register',access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']],


		// RUTAS PARA EL DASHBOARD DEL ADMIN
	[pattern: '/villian/**',       access: ['ROLE_ADMIN']],
	[pattern: '/hero/**',       access: ['ROLE_ADMIN']],
	[pattern: '/usuario/**',       access: ['ROLE_ADMIN']],

	[pattern: '/api/character/**', access: ['ROLE_ADMIN']]
]


grails.plugin.springsecurity.filterChain.chainMap = [
		//unsecured
		[pattern: '/assets/**',      filters: 'none'],
		[pattern: '/**/js/**',       filters: 'none'],
		[pattern: '/**/css/**',      filters: 'none'],
		[pattern: '/**/images/**',   filters: 'none'],
		[pattern: '/**/favicon.ico', filters: 'none'],

		//Stateless chain for REST API
		[
				pattern: '/api/**',
				filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter'
		],

		//Traditional chain for form based login
		[
				pattern: '/**',
				filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'
		]
]

grails.plugin.springsecurity.rest.token.storage.useGorm = true
grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName = "com.mcu.RestAuthenticationtoken"
grails.plugin.springsecurity.rest.token.storage.gorm.tokenValuePropertyName = "token"
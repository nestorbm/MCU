<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'villian.label', default: 'Villian')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
        <g:external dir="css" file="styles.css" />
    </head>
    <body>
    <div class="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
        <h4 style="margin: 0 auto; padding-top: 12px" class="mdl-card__title-text">Current Villians in database</h4>

        <f:table collection="${villianList}" properties="['photo_path','name','description','fights']"/>

        <div class="pagination">
            <g:paginate total="${villianCount ?: 0}" />
        </div>
    </div>

    <div class="demo-separator mdl-cell--1-col"></div>

    <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop blueCard">

        <h4 class="mdl-card__title-text blueCardTitle">Add a Villian!</h4>

        <div style="background-color: white; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px"></div>

        <g:form url="[action:'save',controller:'Villian']" style="padding-top: 15px;padding-left: 10%">
            <fieldset>
                <table cellpadding="4" class="blueTable">
                    <tr>
                        <td style="text-align: right;"><label class="labelBlueForm">Villian Name </label></td>
                        <td><g:textField class="blueTextField" name="villian.name" required=""/><br/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;"><label class="labelBlueForm">Description </label></td>
                        <td><g:textField class="blueTextField" name="villian.description" required=""/><br/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;"><label class="labelBlueForm">Photo </label></td>
                        <td><g:textField class="blueTextField" name="villian.photo_path" required=""/><br/></td>
                        <g:select name="villian.type" from="${com.mcu.TypeCharacter.findByName('Villian')}"
                                  optionKey="id"
                                  disabled="true"
                                  hidden="true"/>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><g:submitButton name="create" class="save blueButton" value="Create" /></td>
                    </tr>
                </table>
            </fieldset>
        </g:form>

    </div>

    </body>
</html>
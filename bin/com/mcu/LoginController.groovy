package com.mcu

import org.springframework.security.access.annotation.Secured
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import grails.plugin.springsecurity.SpringSecurityUtils

@Secured("ROLE_ADMIN")
class LoginController extends SavedRequestAwareAuthenticationSuccessHandler
{
    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response)
    {
        def returnUrl = request.getSession().getAttribute('returnUrl')

        def roles = SpringSecurityUtils.getPrincipalAuthorities()

        for (String role in roles)
        {
            if (role.equals("ROLE_ADMIN")) {
                println ("ADMIN REDIRECTED")
                returnUrl = '/adminHome.gsp'
            }
            else if (role.equals("ROLE_USER")) {
                returnUrl = '/amdinHome.gsp'
            }
            else {
                returnUrl = '/'
            }
        }

        request.getSession().removeAttribute('returnUrl')

        return returnUrl
    }
}
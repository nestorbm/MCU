package com.mcu

class Fight {
    Integer votes_character1
    Integer votes_character2

    static hasOne = [character1:Villian, character2:Hero, battle:Battle]

    static constraints = {
        votes_character1 value: 0
        votes_character2 value: 0
    }
}

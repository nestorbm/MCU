package com.mcu

import grails.rest.Resource
import org.springframework.security.access.annotation.Secured

class Character {

    String name
    String description
    String photo_path

    static hasOne = [type:TypeCharacter]

    static constraints = {
        name nullable: false
        description nullable: false
        photo_path nullable: false
    }
}

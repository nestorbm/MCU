package com.mcu

import org.springframework.security.access.annotation.Secured

class Hero extends Character{

    static hasMany = [fights:Fight]

    static constraints = {
    }
}

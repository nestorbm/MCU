package com.mcu

class TypeCharacter {


    String name

    static hasMany = [costumes:Character]

    static constraints = {
        name nullable: false
    }

    String toString(){
        name
    }
}

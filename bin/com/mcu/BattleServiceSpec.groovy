package com.mcu

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class BattleServiceSpec extends Specification {

    BattleService battleService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Battle(...).save(flush: true, failOnError: true)
        //new Battle(...).save(flush: true, failOnError: true)
        //Battle battle = new Battle(...).save(flush: true, failOnError: true)
        //new Battle(...).save(flush: true, failOnError: true)
        //new Battle(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //battle.id
    }

    void "test get"() {
        setupData()

        expect:
        battleService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Battle> battleList = battleService.list(max: 2, offset: 2)

        then:
        battleList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        battleService.count() == 5
    }

    void "test delete"() {
        Long battleId = setupData()

        expect:
        battleService.count() == 5

        when:
        battleService.delete(battleId)
        sessionFactory.currentSession.flush()

        then:
        battleService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Battle battle = new Battle()
        battleService.save(battle)

        then:
        battle.id != null
    }
}

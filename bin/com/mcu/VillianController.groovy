package com.mcu

import grails.validation.ValidationException
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

class VillianController {

    VillianService villianService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured('ROLE_ADMIN')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Character.findAllByType(TypeCharacter.findByName('Villian')),  model:[villianCount: villianService.count()]
    }

    def show(Long id) {
        respond villianService.get(id)
    }

    def create() {
        respond new Villian(params)
    }
    @Secured('ROLE_ADMIN')
    def save(Villian villian) {
        villian.setType(TypeCharacter.findByName('Villian'))
        if (villian == null) {
            notFound()
            return
        }

        try {
            villianService.save(villian)
        } catch (ValidationException e) {
            respond villian.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'villian.label', default: 'Villian'), villian.id])
                redirect (uri:'/villian/')
            }
            '*' { respond villian, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond villianService.get(id)
    }

    def update(Villian villian) {
        if (villian == null) {
            notFound()
            return
        }

        try {
            villianService.save(villian)
        } catch (ValidationException e) {
            respond villian.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'villian.label', default: 'Villian'), villian.id])
                redirect (uri:'/villian/')
            }
            '*'{ respond villian, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        villianService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'villian.label', default: 'Villian'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'villian.label', default: 'Villian'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

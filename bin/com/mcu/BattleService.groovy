package com.mcu

import grails.gorm.services.Service

@Service(Battle)
interface BattleService {

    Battle get(Serializable id)

    List<Battle> list(Map args)

    Long count()

    void delete(Serializable id)

    Battle save(Battle battle)

}
package com.mcu

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class TypeCharacterController {

    TypeCharacterService typeCharacterService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond typeCharacterService.list(params), model:[typeCharacterCount: typeCharacterService.count()]
    }

    def show(Long id) {
        respond typeCharacterService.get(id)
    }

    def create() {
        respond new TypeCharacter(params)
    }

    def save(TypeCharacter typeCharacter) {
        if (typeCharacter == null) {
            notFound()
            return
        }

        try {
            typeCharacterService.save(typeCharacter)
        } catch (ValidationException e) {
            respond typeCharacter.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'typeCharacter.label', default: 'TypeCharacter'), typeCharacter.id])
                redirect typeCharacter
            }
            '*' { respond typeCharacter, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond typeCharacterService.get(id)
    }

    def update(TypeCharacter typeCharacter) {
        if (typeCharacter == null) {
            notFound()
            return
        }

        try {
            typeCharacterService.save(typeCharacter)
        } catch (ValidationException e) {
            respond typeCharacter.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'typeCharacter.label', default: 'TypeCharacter'), typeCharacter.id])
                redirect typeCharacter
            }
            '*'{ respond typeCharacter, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        typeCharacterService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'typeCharacter.label', default: 'TypeCharacter'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'typeCharacter.label', default: 'TypeCharacter'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

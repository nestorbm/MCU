package com.mcu

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class FightServiceSpec extends Specification {

    FightService fightService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Fight(...).save(flush: true, failOnError: true)
        //new Fight(...).save(flush: true, failOnError: true)
        //Fight fight = new Fight(...).save(flush: true, failOnError: true)
        //new Fight(...).save(flush: true, failOnError: true)
        //new Fight(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //fight.id
    }

    void "test get"() {
        setupData()

        expect:
        fightService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Fight> fightList = fightService.list(max: 2, offset: 2)

        then:
        fightList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        fightService.count() == 5
    }

    void "test delete"() {
        Long fightId = setupData()

        expect:
        fightService.count() == 5

        when:
        fightService.delete(fightId)
        sessionFactory.currentSession.flush()

        then:
        fightService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Fight fight = new Fight()
        fightService.save(fight)

        then:
        fight.id != null
    }
}

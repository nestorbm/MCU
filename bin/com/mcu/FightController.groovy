package com.mcu

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class FightController {

    FightService fightService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond fightService.list(params), model:[fightCount: fightService.count()]
    }

    def show(Long id) {
        respond fightService.get(id)
    }

    def create() {
        respond new Fight(params)
    }

    def save(Fight fight) {
        if (fight == null) {
            notFound()
            return
        }

        try {
            fightService.save(fight)
        } catch (ValidationException e) {
            respond fight.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'fight.label', default: 'Fight'), fight.id])
                redirect fight
            }
            '*' { respond fight, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond fightService.get(id)
    }

    def update(Fight fight) {
        if (fight == null) {
            notFound()
            return
        }

        try {
            fightService.save(fight)
        } catch (ValidationException e) {
            respond fight.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'fight.label', default: 'Fight'), fight.id])
                redirect fight
            }
            '*'{ respond fight, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        fightService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'fight.label', default: 'Fight'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'fight.label', default: 'Fight'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

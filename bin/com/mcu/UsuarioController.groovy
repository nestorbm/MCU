package com.mcu

import grails.plugin.springsecurity.SpringSecurityUtils
import grails.rest.Resource
import grails.validation.ValidationException
import org.springframework.boot.actuate.autoconfigure.ManagementServerProperties
import org.springframework.security.access.annotation.Secured
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import static org.springframework.http.HttpStatus.*
import grails.rest.RestfulController

@Resource(uri='/api/usuarioController')
@Secured (['ROLE_ADMIN'])
class UsuarioController extends RestfulController{

    static responseFormats = ['json', 'xml']

    UsuarioController(){
        super(Usuario)
    }

    UsuarioService usuarioService
    UsuarioRoleService usuarioRoleService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured (['ROLE_ADMIN'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond usuarioService.list(params), model:[usuarioCount: usuarioService.count()]
    }

    def show(Long id) {
        respond usuarioService.get(id)
    }

    def create() {
        respond new Usuario(params)
    }


    @Secured (['IS_AUTHENTICATED_ANONYMOUSLY'])
    def save(Usuario usuario) {
        if (usuario == null) {
            notFound()
            return
        }

        try {
            usuarioService.save(usuario)
            Role role = Role.findByAuthorityLike('ROLE_USER')
            usuarioRoleService.save(UsuarioRole.create(usuario,role))

        } catch (ValidationException e) {
            respond usuario.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuario.username])+" exitosamente, ingrese sus datos para continuar"
                redirect (uri:'/')
            }
            '*' { respond "Usuario creado exitosamente", [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond usuarioService.get(id)
    }

    def update(Usuario usuario) {
        if (usuario == null) {
            notFound()
            return
        }

        try {
            usuarioService.save(usuario)
        } catch (ValidationException e) {
            respond usuario.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuario.id])
                redirect usuario
            }
            '*'{ respond usuario, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        usuarioService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}

package mcu

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/redirect"(view:"/redirect")

        "/"(view:"/index")
        "/adminHome"(view:"/adminHome")
        "/usuario/register"(view:"/usuario/register")

        "/battle"(view:"/battle/")

        "500"(view:'/error')
        "404"(view:'/notFound')

        "/api/character"(controller: "character",action: "index")
    }
}

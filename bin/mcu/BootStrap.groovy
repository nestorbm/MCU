package mcu

import com.mcu.*

class BootStrap {

    def init = { servletContext ->

        Usuario admin = new Usuario (username: 'admin',password: 'admin123').save()

        Role roleAdmin = new Role(authority: 'ROLE_ADMIN').save()
        UsuarioRole.create(admin,roleAdmin)

        Usuario user = new Usuario (username:'nestorbm',password: 'nestor123').save()
        Role userRole = new Role (authority: 'ROLE_USER').save()

        UsuarioRole.create(user,userRole)

        TypeCharacter heroCharacter = new TypeCharacter(name: 'Hero').save()
        TypeCharacter antiHeroCharacter = new TypeCharacter(name: 'Anti Hero').save()
        TypeCharacter villiancharacter = new TypeCharacter(name:'Villian').save()

        def deadPool = new Hero(name: "Deadpool",description: "Wade Wilson",photo_path: "nope",type: antiHeroCharacter).save()
        def ironMan = new Hero(name: "Iron Man",description: "Tony Stark",photo_path: "no tony",type: heroCharacter).save()
        def ultron = new Villian (name:"Ultron",description: "No idea",photo_path: "no ultron",type: villiancharacter).save()
    }
    def destroy = {
    }
}

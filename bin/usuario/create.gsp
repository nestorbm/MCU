<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
    </head>
    <body>
        <a href="#create-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div id="create-usuario" class="content scaffold-create" role="main">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="ROLE_USER">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.usuario}">
            <ul class="errors" role="ROLE_USER">
                <g:eachError bean="${this.usuario}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>

            <g:form resource="${this.usuario}" method="POST" action="save">
                <fieldset>
                    <p class="info">
                        Complete the form below to create an account!
                    </p>
                    <g:hasErrors bean="${usuario}">
                        <div class="errors">
                            <g:renderErrors bean="${usuario}"/>
                        </div>
                    </g:hasErrors>
                    <p>
                        <label for="username">Username</label>
                        <g:textField name="username" value="${usuario?.username}"
                                     class="${hasErrors(bean:usuario,field:'username','errors')}"/>
                    </p>
                    <p>
                        <label for="password">Password</label>
                        <g:passwordField name="password"
                                         class="${hasErrors(bean:usuario,field:'password','errors')}" />
                    </p>
                    <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>

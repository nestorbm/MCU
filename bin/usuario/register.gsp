<!DOCTYPE html>
<html lang="en">
<head>
    <title><g:message code='springSecurity.login.title'/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <g:external dir="css" file="util.css" />
    <g:external dir="css" file="main.css" />

</head>
<body>
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div id="login" class="login100-form">
                <div class="inner">
                    <span class="login100-form-title p-b-26">
                        Sign up into MCU!
                    </span>

                    <br>

                    <g:if test='${flash.message}'>
                        <div class="login_message">${flash.message}</div>
                    </g:if>


                    <g:form resource="${this.usuario}" method="POST" action="save" class="login100-form" autocomplete="off">

                        <g:hasErrors bean="${usuario}">
                            <div class="errors">
                                <g:renderErrors bean="${usuario}"/>
                            </div>
                        </g:hasErrors>

                        <div class="wrap-input100">
                            <label for="username"><g:message code='springSecurity.login.username.label'/>:</label>
                            <g:textField id="username" name="username" value="${usuario?.username}"
                                         class="${hasErrors(bean:usuario,field:'username','errors')} input100"/>
                        </div>

                        <div class="wrap-input100">
                            <label for="password"><g:message code='springSecurity.login.password.label'/>:</label>
                            <g:passwordField id="password" name="password"
                                             class="${hasErrors(bean:usuario,field:'password','errors')} input100" />
                        </div>


                        <div class="container-login100-form-btn">
                            <p class = "wrap-login100-form-btn">
                                <p class = "login100-form-bgbtn"></p>
                        <g:submitButton style="background: #5bc0de" class="login100-form-btn" name="create" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                            </p>
                        </div>

                        <br>
                        <br>

                    </g:form>
                </div>
            </div>
        </div>
        <script>
            (function() {
                document.forms['loginForm'].elements['${usernameParameter ?: 'username'}'].focus();
            })();
        </script>

        <!--===============================================================================================-->
        <asset:javascript src="jquery-2.2.0.min.js"/>

        <asset:javascript src="bootstrap.min.js"/>

        <asset:javascript src="main.js"/>

</body>
</html>
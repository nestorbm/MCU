<!DOCTYPE html>
<html lang="en">
<head>
	<title><g:message code='springSecurity.login.title'/></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<g:external dir="css" file="util.css" />
	<g:external dir="css" file="main.css" />

</head>
<body>
<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
            <div id="login" class="login100-form">
                <div class="inner">
                    <span class="login100-form-title p-b-26">
                        Welcome to MCU!
                    </span>

                    <br>

                    <g:if test='${flash.message}'>
                        <div style="color: #5bc0de" class="login_message">${flash.message}</div>
                    </g:if>
                    <br>
                    <br>

                    <form action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" class="login100-form" autocomplete="off">
                        <g:external dir="images" file="user.jpg" class="demo-avatar"/>
                        <div class="wrap-input100">
                            <label for="username"><g:message code='springSecurity.login.username.label'/>:</label>
                            <input type="text" class="input100" name="${usernameParameter ?: 'username'}" id="username"/>
                        </div>

                        <div class="wrap-input100">
                            <label for="password"><g:message code='springSecurity.login.password.label'/>:</label>
                            <input type="password" class="input100" name="${passwordParameter ?: 'password'}" id="password"/>
                        </div>

                        <center>

                        <div id="remember_me_holder">
                            <input type="checkbox" class="chk" name="${rememberMeParameter ?: 'remember-me'}" id="remember_me" <g:if test='${hasCookie}'>checked="checked"</g:if>/>
                            <label for="remember_me"><g:message code='springSecurity.login.remember.me.label'/></label>
                        </div>
                        </center>

                        <div class="container-login100-form-btn">
                            <p class = "wrap-login100-form-btn">
                            <p class = "login100-form-bgbtn"></p>
                                <input style="background: #5bc0de" class="login100-form-btn" type="submit" id="submit" value="${message(code: 'springSecurity.login.button')}"/>
                            </p>
                        </div>

                        <br>
                        <br>

                        <center>

                            <span class="txt1">
                                Don’t have an account?
                            </span>

                            <a class="txt2" href="/usuario/register">
                                Sign Up
                            </a>

                        </center>

                    </form>
                </div>
            </div>
		</div>
<script>
	(function() {
		document.forms['loginForm'].elements['${usernameParameter ?: 'username'}'].focus();
	})();
</script>

<!--===============================================================================================-->
<asset:javascript src="jquery-2.2.0.min.js"/>

<asset:javascript src="bootstrap.min.js"/>

<asset:javascript src="main.js"/>

</body>
</html>
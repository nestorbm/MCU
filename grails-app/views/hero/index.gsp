<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'hero.label', default: 'Hero')}" />
        <g:external dir="css" file="styles.css" />
        <g:external dir="css" file="styles.css" />
    </head>
    <body>
    <div class="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
        <h4 style="margin: 0 auto; padding-top: 12px" class="mdl-card__title-text">Current Heroes in database</h4>
        <f:table collection="${heroList}" properties="['photo_path','name','description','fights']"/>
    </div>

    <div class="demo-separator mdl-cell--1-col"></div>

    <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-desktop blueCard">

        <h4 class="mdl-card__title-text blueCardTitle">Add a Hero!</h4>

        <div style="background-color: white; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px"></div>

        <g:form url="[action:'save',controller:'Hero']" style="padding-top: 15px;padding-left: 10%">
            <fieldset>
                <table cellpadding="4" class="blueTable">
                    <tr>
                        <td style="text-align: right;"><label class="labelBlueForm">Hero Name </label></td>
                        <td><g:textField class="blueTextField" name="hero.name" required=""/><br/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;"><label class="labelBlueForm">Description </label></td>
                        <td><g:textField class="blueTextField" name="hero.description" required=""/><br/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;"><label class="labelBlueForm">Photo </label></td>
                        <td><g:textField style="border-radius: 6px" name="hero.photo_path" required=""/><br/></td>
                        <g:select name="hero.type" from="${com.mcu.TypeCharacter.findByName('Hero')}"
                                  optionKey="id"
                                  disabled="true"
                                  hidden="true"/>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><g:submitButton name="create" class="save blueButton" value="Create" /></td>
                    </tr>
                </table>
            </fieldset>
        </g:form>

    </div>
    <exa:datatable id="table1" items="${heroList}" />
    </body>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var datatable = Exa.Datatable.getDatatable('table2');
        $('#table1 tbody').on('click', '.btn-alert', function () {
            var data = datatable.getInstance().row($(this).parents('tr')).data();
            alert("Gender: " + data['sex']);
        });
    });
</script>

</html>
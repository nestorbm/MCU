<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <div class="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
        <h4 style="margin: 0 auto; padding-top: 12px" class="mdl-card__title-text">Current Users in database</h4>

        <f:table collection="${usuarioList}" properties="['username','battles','accountLocked']"/>

        <div class="pagination">
            <g:paginate total="${usuarioCount ?: 0}" />
        </div>
    </div>


    <div class="demo-separator mdl-cell--1-col"></div>

    <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop blueCard">

        <h4 style="margin: 0 auto; padding-top: 12px" class="mdl-card__title-text blueCardTitle">Add a User!</h4>
        <div style="background-color: white; height: 4px; width: 80%; margin: 0 auto; margin-top: 8px"></div>

        <g:form url="[action:'save',controller:'Usuario']" style="padding-top: 15px;padding-left: 10%">
            <fieldset>
                <table cellpadding="4" class="blueTable">
                    <tr>
                        <td style="text-align: right;"><label class="labelBlueForm">Username </label></td>
                        <td><g:textField name="usuario.username" class="blueTextField" required=""/><br/></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;"><label class="labelBlueForm">Password </label></td>
                        <td><g:passwordField name="usuario.password" class="blueTextField" required=""/><br/></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><g:submitButton name="create" class="save blueButton" value="Create" /></td>
                    </tr>
                </table>
            </fieldset>
        </g:form>

    </div>

    </body>
</html>
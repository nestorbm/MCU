package mcu

import com.mcu.*

class BootStrap {

    def init = { servletContext ->

        Usuario admin = new Usuario (username: 'admin',password: 'admin123').save()

        Role roleAdmin = new Role(authority: 'ROLE_ADMIN').save()
        UsuarioRole.create(admin,roleAdmin)

        Usuario user = new Usuario (username:'nestor.bernal96@gmail.com',password: 'nestor123').save()
        Role userRole = new Role (authority: 'ROLE_USER').save()

        def api = new Usuario(username:'api',password:'MCUapi2018funcional').save()
        Role apiRole = new Role (authority: 'ROLE_API').save()

        UsuarioRole.create(user,userRole)
        UsuarioRole.create(api,apiRole)

        TypeCharacter heroCharacter = new TypeCharacter(name: 'Hero').save()
        TypeCharacter antiHeroCharacter = new TypeCharacter(name: 'Anti Hero').save()
        TypeCharacter villiancharacter = new TypeCharacter(name:'Villian').save()

        def deadPool = new Character (name: "Deadpool",description: "Wade Wilson, also known as the Merc with a Mouth, also known as Deadpool, is incredibly powerful. A skilled martial artist, he underwent a horrific experiment and came out with incredible powers including advanced regeneration, superhuman strength, stamina, durability, and speed. Because of his healing abilities, he’s virtually immortal.",photo_path: "nope",type: heroCharacter).save()
        def ironMan = new Character(name: "Iron Man",description: "Tony Stark",photo_path: "no tony",type: heroCharacter).save()
        def ultron = new Character (name:"Ultron",description: "Ultron's abilities vary with each redesign, but typically include superhuman strength and durability, the power of flight, and various offensive weapons such as concussion blasters, radiation emitters and his \"encephalo-ray\", which plunges its victims into a deathlike coma. The latter ray also allows Ultron to mesmerize and outright mind-control his victims, or implant subliminal hypnotic commands within their minds to be enacted at a later time. Ultron's outer shell is usually composed of Adamantium, rendering it almost totally impervious to damage; however, his internal mechanisms are generally less durable and more easily damaged. Ultron's Adamantium forms have proven vulnerable to molecular rearrangement devices and the metal-destabilizing ore known as Savage Land Vibranium",photo_path: "no ultron",type: villiancharacter).save()

        def hulk = new Character(name: "Hulk",description: "Bruce Banner became the Hulk when he was exposed to high amounts of gamma radiation. Whenever he becomes angry, he transforms into the green beast. The Hulk possesses limitless physical strength with nigh-invulnerability. As Bruce Banner, he has an incredible intellect and is the most talented nuclear scientist on the planet.",photo_path: "no hulk",type: heroCharacter).save()

        def magneto = new Character(name: "Magneto",
                description: "Magneto is one of Marvel’s most powerful super villains. " +
                        "After witnessing his family being murdered by Nazis, " +
                        "Max Eisenhardt was sent to Auschwitz but later became the super-villain known as Magneto. " +
                        "With the ability to manipulate metal and control it at will, he can virtually control anything " +
                        "(you’d be surprised the things that contain metal in them). " +
                        "His powers of magnetism are so strong, he was able to pull Wolverine’s adamantium claws " +
                        "right out of his body.",photo_path: "no hulk",type: villiancharacter).save()

        def Apocalypse = new Character (name:"Apocalypse",description: "Five thousand years ago, En Sabah Nur was born in Ancient Egypt. He grew to be a great conqueror during his time and an adversary to the X-Men and Marvel superheroes. Apocalypse is near immortal, living thousands of years, but his super-strength is one of his main great attributes being able to even restrain the Hulk himself.",photo_path: "no ultron",type: villiancharacter).save()
        def vulcan = new Character (name:"Vulcan",description: "Gabriel Summers was the unborn brother of Scott (Cyclops) and Alex Summers (Havoc). Gabriel was abducted by D’Ken, removing him from his mother’s body. Eventually, he came under the tutelage of Professor Xavier. Taking the name Vulcan, he excels at energy manipulation and energy blasts. He can also siphon others’ energy and use his energy into force fields.",photo_path: "no ultron",type: heroCharacter).save()

        def silverSurfer = new Character (name:"Silver Surfer",description: "Born on the planet Zenn-La, Norrin Radd helped fight the battle of their invader, Galactus. Making a deal with Galactus, Radd became the Silver Surfer, a slave to Galactus. Silver Surfer is one of the most powerful beings in the Marvel universe. His main power is the Power Cosmic, giving him the ability to manipulate energy.",photo_path: "no ultron",type: villiancharacter).save()

        def thor = new Character (name:"Thor",description: "Thor is the son of Odin and the wielder of Mjolnir, a powerful hammer. Thor has incredible strength and speed, being able to combat the mightiest of foes. From flight to the ability to control the Earth, Thor isn’t usually a hero you want to mess with.",photo_path: "no ultron",type: heroCharacter).save()

        def thanos = new Character (name:"Thanos",description: "The last son of A’Lars, Thanos became obsessed with death at a young age. With increasing power, he grew an appetite for conquest. He waged a penultimate war against the Avengers. Many of his abilities are unmatched, including strength, intelligence, durability, and speed. Add his energy manipulation, telepathy, and immortality, and he becomes a foe nigh impossible to defeat.",photo_path: "none",type: villiancharacter).save()

        def odin = new Character(name: "Odin",description: "Odin is one of the most powerful of all gods and is called the All-Father. His intelligence, speed, durability, and energy projection are superior than most. With the ability of Odinforce, he has unspeakable and endless power.",photo_path: "no",type: heroCharacter).save()

        def wolverine = new Character(name: "Wolverine",description: "Born with super-human senses and the power to heal from almost any wound, Wolverine was captured by a secret Canadian organization and given an unbreakable skeleton and claws. Treated like an animal, it took years for him to control himself. Now, he's a premiere member of both the X-Men and the Avengers.",photo_path: "no tony",type: heroCharacter).save()

        def captainAmerica = new Character(name: "Captain America",description: "Vowing to serve his country any way he could, young Steve Rogers took the super soldier serum to become America's one-man army. Fighting for the red, white and blue for over 60 years, Captain America is the living, breathing symbol of freedom and liberty",photo_path: "no tony",type: heroCharacter).save()

        def blackWidow = new Character(name: "Black Widow",description: "Natasha Romanova, known by many aliases, is an expert spy, athlete, and assassin. Trained at a young age by the KGB's infamous Red Room Academy, the Black Widow was formerly an enemy to the Avengers. She later became their ally after breaking out of the U.S.S.R.'s grasp, and also serves as a top S.H.I.E.L.D. agent",photo_path: "no tony",type: heroCharacter).save()

        def blackPanther = new Character(name: "Black Panther",description: "T'Challa is a brilliant tactician, strategist, scientist, tracker and a master of all forms of unarmed combat whose unique hybrid fighting style incorporates acrobatics and aspects of animal mimicry. T'Challa being a royal descendent of a warrior race is also a master of armed combat, able to use a variety of weapons but prefers unarmed combat. He is a master planner who always thinks several steps ahead and will go to extreme measures to achieve his goals and protect the kingdom of Wakanda",photo_path: "no tony",type: heroCharacter).save()

        def storm = new Character(name: "Storm",description: "Ororo Monroe is the descendant of an ancient line of African priestesses, all of whom have white hair, blue eyes, and the potential to wield magic.",photo_path: "no tony",type: heroCharacter).save()

        def xavier = new Character(name: "Professor X",description: "Professor X is a mutant who possesses vast psionic powers, making him arguably the world's most powerful telepath. He can read minds and project his own thoughts into the minds of others within a radius of approximately 250 miles. With extreme effort, he can greatly extend that radius. Professor X can also psionically manipulate the minds of others, for example to make himself seem invisible, and project illusions into them. He can also induce temporary mental and/or physical paralysis, loss of particular memories or even total amnesia. Within close range, Professor X can manipulate almost any number of minds for such simple feats",photo_path: "no tony",type: heroCharacter).save()

        def strange = new Character(name: "Doctor Strange",description: "Doctor Strange is one of the most powerful sorcerers in existence. Like most sorcerers, he draws his power from three primary sources: the invocation of powerful mystic entities or objects, the manipulation of the universe's ambient magical energy, and his own psychic resources. Strange's magical repertoire includes energy projection and manipulation, matter transformation, animation of inanimate objects, teleportation, illusion-casting, mesmerism, thought projection, astral projection, dimensional travel, time travel and mental possession, to name a few. The full range of his abilities is unknown.",photo_path: "no tony",type: heroCharacter).save()

        def jean = new Character(name: "Jean Grey",description: " Jean Grey possessed telepathic powers enabling her to read minds, project her thoughts into the minds of others, initiate astral travel, and mentally stun opponents with pure psionic force, among other talents. She also possessed telekinesis, allowing her to levitate and manipulate objects and others, generate force fields, fly, and stimulate heat molecules to generate concussive blasts.Her powers were magnified to near-infinite levels while she served as an avatar for the cosmic Phoenix Force. She was able to manipulate matter and energy on a molecular scale, although this varied on the Force's status and how much power it chose to allocate to her",photo_path: "no tony",type: heroCharacter).save()

        def doctorDoom = new Character (name:"Doctor Doom",description: "Doom can exchange minds with others. He possesses some mystical abilities, such as casting bolts of eldritch energy and invoking mystical entities (principalities) for additional support. While empowered by the Haazareth, his mystical powers were on a par with those of Dr. Strange.",photo_path: "no ultron",type: villiancharacter).save()

        def loki = new Character (name:"Loki",description: "Loki possesses physical abilities far superior to humans, an increased lifespan, superhuman strength (able to lift up to 50 tons), immune to terrestrial diseases, and resistant to conventional injury.Loki is perhaps the most powerful sorcerer in all of Asgard. His many magical abilities included shape-shifting (able to gain the basic natural abilities inherent in each form), astral projection, molecular rearrangement, eldritch energy blasts, illusion casting, flight (via levitation), telepathy, hypnosis, and teleportation.Loki is immune to most physical injury, and can reattach severed body parts, including his own head. Loki can mystically imbue objects or beings with specific but temporary powers, and enhance the powers of superhumans. Loki can also magically create rifts between dimensions, allowing him or other objects passage from one universe to another. Most often this rift is between Asgard and Earth",photo_path: "no ultron",type: villiancharacter).save()

        def venom = new Character (name:"Venom",description: "The symbiotic suit bonded to Brock was designed as a protoplasmic dip that would restore and rebuild damaged body tissues. As such, it grants him tremendous regenerative abilities. The suit also greatly augments his physical abilities, projects organic “webbing”, and is capable of limited shapeshifting",photo_path: "no ultron",type: villiancharacter).save()

        def juggernaut = new Character (name:"Juggernaut",description: "The Juggernaut originally possessed untold mystical power which enhanced his strength to an unknown degree and made him a seemingly irresistible, unstoppable being. Once he began to walk in a given direction, no obstacle or force on Earth was able to stop him. Some obstacles, for example many tons of rock, or forces such as plasma-discharge cannons slowed his pace considerably, but nothing stopped him permanently from advancing. The Juggernaut was, however, vulnerable to magical forces of sufficient strength.Besides giving him vast superhuman strength, the mystical energy of Cyttorak gave the Juggernaut an extraordinary degree of resistance to all forms of injury. The Juggernaut could also shield himself even further from injury by mentally surrounding himself with a force field. Enveloped by this field, the Juggernaut had been seen to survive the fiery explosion of a truck transporting a huge quantity of oil without any injury whatsoever.",photo_path: "no ultron",type: villiancharacter).save()


        def fav1 = new Favorite (user: user,character: strange).save()
        def fav2 = new Favorite (user: user,character: ironMan).save()
        def fav3 = new Favorite (user: user,character: juggernaut).save()


        println("All initial data stored scusefully")

    }
    def destroy = {
    }
}

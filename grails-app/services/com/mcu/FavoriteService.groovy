package com.mcu

import grails.gorm.services.Service

@Service(Favorite)
interface FavoriteService {

    Favorite get(Serializable id)

    List<Favorite> list(Map args)

    Long count()

    void delete(Serializable id)

    Favorite save(Favorite favorite)

}
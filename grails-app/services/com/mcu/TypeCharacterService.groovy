package com.mcu

import grails.gorm.services.Service

@Service(TypeCharacter)
interface TypeCharacterService {

    TypeCharacter get(Serializable id)

    List<TypeCharacter> list(Map args)

    Long count()

    void delete(Serializable id)

    TypeCharacter save(TypeCharacter typeCharacter)

}
package com.mcu

import grails.gorm.services.Service

@Service(Fight)
interface FightService {

    Fight get(Serializable id)

    List<Fight> list(Map args)

    Long count()

    void delete(Serializable id)

    Fight save(Fight fight)

}
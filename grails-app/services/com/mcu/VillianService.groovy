package com.mcu

import grails.gorm.services.Service

@Service(Villian)
interface VillianService {

    Villian get(Serializable id)

    List<Villian> list(Map args)
    Long count()

    void delete(Serializable id)

    Villian save(Villian villian)

}
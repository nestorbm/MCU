package com.mcu

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import com.mcu.*;

class FightController {

    FightService fightService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", fight:"PUT", getFightInfo:"PUT"]

    @Secured (['ROLE_USER'])
    def index(Integer max) {

        def fights = Fight.executeQuery("select C.name, F.battle.id from Fight as F, Character as C ,Battle as B where C.id = F.character.id and B.id = F.battle.id and B.deadline > current_date order by F.id")

        respond  fights
    }

    @Secured (['ROLE_USER'])
    def myCurrentFights (){
        def userID = params.userID
        def fights = Fight.executeQuery("select C.name, F.battle.id from Fight as F, Character as C ,Battle as B, Usuario as U where C.id = F.character.id and B.id = F.battle.id and U.id = B.user.id and U.id ="+userID+" and B.deadline > current_date order by F.id")
        respond fights
    }

    @Secured (['ROLE_USER'])
    def myEndedFights (){
        def userID = params.userID
        def fights = Fight.executeQuery("select C.name, F.battle.id from Fight as F, Character as C ,Battle as B, Usuario as U where C.id = F.character.id and B.id = F.battle.id and U.id = B.user.id and U.id ="+userID+" and B.deadline <= current_date order by F.id")
        respond fights
    }

    @Secured (['ROLE_USER'])
    def getFightInfo(){
        def fightID = params.fightID
        def fights = Fight.executeQuery("select C.name, C.description, F.battle.id, C.id from Fight as F, Character as C where C.id = F.character and F.battle.id ="+fightID+" order by F.id")
        respond fights
    }

    def show(Long id) {
        respond fightService.get(id)
    }

    def create() {
        respond new Fight(params)
    }

    @Secured (['ROLE_USER'])
    def fight (){
        def char1ID = params.character1
        def char2ID = params.character2

        def char1 = Character.findById(char1ID)
        def char2 = Character.findById(char2ID)


        def battle = Battle.findById(params.battle)


        Fight newFight = new Fight(battle: battle.id, character: char1.id)
        Fight second = new Fight(battle: battle.id, character: char2.id)

        fightService.save(newFight)
        fightService.save(second)

        respond Fight.findAllByBattle(battle)
    }

    @Secured (['ROLE_USER'])
    def save(Fight fight) {
        if (fight == null) {
            notFound()
            return
        }

        try {
            fightService.save(fight)
        } catch (ValidationException e) {
            respond fight.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'fight.label', default: 'Fight'), fight.id])
                redirect fight
            }
            '*' { respond fight, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond fightService.get(id)
    }

    def update(Fight fight) {
        if (fight == null) {
            notFound()
            return
        }

        try {
            fightService.save(fight)
        } catch (ValidationException e) {
            respond fight.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'fight.label', default: 'Fight'), fight.id])
                redirect fight
            }
            '*'{ respond fight, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        fightService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'fight.label', default: 'Fight'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'fight.label', default: 'Fight'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

package com.mcu

import grails.validation.ValidationException
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.CREATED

class RegisterController {

    UsuarioService usuarioService
    UsuarioRoleService usuarioRoleService

    @Secured (['permitAll'])
    def nuevoUsuario(Usuario usuario) {
        println("nuevo usuario")
        if (usuario == null) {
            notFound()
            return
        }

        try {
            usuarioService.save(usuario)
            Role role = Role.findByAuthorityLike('ROLE_USER')
            usuarioRoleService.save(UsuarioRole.create(usuario,role))

        } catch (ValidationException e) {
            respond usuario.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuario.username])+" exitosamente, ingrese sus datos para continuar"
                redirect (uri:'/')
            }
            '*' { respond usuario, [status: CREATED] }
        }
    }
}

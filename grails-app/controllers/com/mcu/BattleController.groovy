package com.mcu

import grails.validation.ValidationException
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*

class BattleController {

    BattleService battleService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured('ROLE_USER')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def battles = Battle.executeQuery("select B from Battle as B where deadline > current_date")
        respond battles
    }

    @Secured(['ROLE_USER'])
    def battlesEnded (){
        def battles = Battle.executeQuery("select C.name, B.id , B.deadline from Battle as B, Character as C, Fight as F where B.id = F.battle.id and F.character.id = C.id and B.deadline <= current_date")
        respond battles
    }

    def show(Long id) {
        respond battleService.get(id)
    }

    @Secured (['ROLE_USER'])
    def create() {
        respond new Battle(params)
    }

    @Secured (['ROLE_USER'])
    def save(Battle battle) {
        if (battle == null) {
            notFound()
            return
        }

        try {
            battleService.save(battle)
        } catch (ValidationException e) {
            respond battle.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'battle.label', default: 'Battle'), battle.id])
                redirect battle
            }
            '*' { respond battle, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond battleService.get(id)
    }

    def update(Battle battle) {
        if (battle == null) {
            notFound()
            return
        }

        try {
            battleService.save(battle)
        } catch (ValidationException e) {
            respond battle.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'battle.label', default: 'Battle'), battle.id])
                redirect battle
            }
            '*'{ respond battle, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        battleService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'battle.label', default: 'Battle'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'battle.label', default: 'Battle'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

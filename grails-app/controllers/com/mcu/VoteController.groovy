package com.mcu

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class VoteController {

    VoteService voteService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", userVotes: "PUT"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond voteService.list(params), model:[voteCount: voteService.count()]
    }

    def show(Long id) {
        respond voteService.get(id)
    }

    @Secured (['ROLE_USER'])
    def create() {
        respond new Vote(params)
    }

    @Secured (['ROLE_USER'])
    def save(Vote vote) {
        if (vote == null) {
            notFound()
            return
        }

        try {
            voteService.save(vote)
        } catch (ValidationException e) {
            respond vote.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'vote.label', default: 'Vote'), vote.id])
                redirect vote
            }
            '*' { respond vote, [status: OK] }
        }
    }

    @Secured (['ROLE_USER'])
    def userVotes(){
        def userID = params.userID;
        def battleID = params.battleID;

        def user = Usuario.findById(userID)
        def battle = Battle.findById(battleID)

        def userVotes = Vote.findAllWhere(user:user,battle: battle)

        respond userVotes
    }

    @Secured(['ROLE_USER'])
    def getBattleVotes(){
        def battleID = params.battleID;
        def votes = Vote.executeQuery("select sum(v.votes_character1) as vch1, sum(v.votes_character2) as vch2 from Vote as v, Battle as b where v.battle.id = b.id and b.id ="+battleID)
        respond votes

    }

    def edit(Long id) {
        respond voteService.get(id)
    }

    def update(Vote vote) {
        if (vote == null) {
            notFound()
            return
        }

        try {
            voteService.save(vote)
        } catch (ValidationException e) {
            respond vote.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'vote.label', default: 'Vote'), vote.id])
                redirect vote
            }
            '*'{ respond vote, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        voteService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'vote.label', default: 'Vote'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'vote.label', default: 'Vote'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

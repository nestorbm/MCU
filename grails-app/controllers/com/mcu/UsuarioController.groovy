package com.mcu


import grails.validation.ValidationException
import groovy.json.JsonSlurper
import org.apache.commons.lang.RandomStringUtils
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.rest.RestfulController

class UsuarioController {

    UsuarioService usuarioService
    UsuarioRoleService usuarioRoleService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured (['ROLE_ADMIN'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond usuarioService.list(params), model:[usuarioCount: usuarioService.count()]
    }

    @Secured(['ROLE_API'])
    def findID (){
        respond Usuario.findByUsername(params.username)
    }

    @Secured(['ROLE_API'])
    def changePassword(){
        def object = null
        try {
            def changePass = Usuario.findByUsername(params.username)

            int randomStringLength = 32
            String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join()
            String randomString = RandomStringUtils.random(randomStringLength, charset.toCharArray())
            changePass.setPassword(randomString)

            usuarioService.save(changePass)
            def jsonSlurper = new JsonSlurper()
            object = jsonSlurper.parseText('{ "newPass": "'+randomString+'" }')

        }catch(Exception e){
            println("No user found")
        }
        respond object
    }

    def show(Long id) {
        respond usuarioService.get(id)
    }

    def create() {
        respond new Usuario(params)
    }

    @Secured (['IS_AUTHENTICATED_ANONYMOUSLY','ROLE_API'])
    def save(Usuario usuario) {
        if (usuario == null) {
            notFound()
            return
        }

        try {
            usuarioService.save(usuario)
            Role role = Role.findByAuthorityLike('ROLE_USER')
            usuarioRoleService.save(UsuarioRole.create(usuario,role))

        } catch (ValidationException e) {
            respond usuario.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuario.username])+" exitosamente, ingrese sus datos para continuar"
                redirect (uri:'/')
            }
            '*' { respond "Usuario creado exitosamente", [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond usuarioService.get(id)
    }


    @Secured(['ROLE_API','IS_AUTHENTICATED_ANONYMOUSLY'])
    def update(Usuario usuario) {
        if (usuario == null) {
            notFound()
            return
        }

        try {
            usuarioService.save(usuario)
        } catch (ValidationException e) {
            respond usuario.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuario.id])
            }
            respond usuario
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        usuarioService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'usuario.label', default: 'Usuario'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}

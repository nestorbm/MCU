package com.mcu

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class FavoriteController {

    FavoriteService favoriteService

    static allowedMethods = [save: "POST", update: "PUT", delete: "PUT", myFavorites:"PUT"]

    @Secured (['ROLE_USER'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond favoriteService.list(params), model:[favoriteCount: favoriteService.count()]
    }

    @Secured (['ROLE_USER'])
    def myFavorites (){
        def user = params.userID
        def myFavorites = Favorite.executeQuery("select C.id, C.name , C.description from Favorite as F, Character as C where C.id = F.character.id and F.user.id = "+user)
        respond myFavorites
    }

    def show(Long id) {
        respond favoriteService.get(id)
    }


    def create() {
        respond new Favorite(params)
    }


    @Secured (['ROLE_USER'])
    def save(Favorite favorite) {
        if (favorite == null) {
            notFound()
            return
        }

        try {
            favoriteService.save(favorite)
        } catch (ValidationException e) {
            respond favorite.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'favorite.label', default: 'Favorite'), favorite.id])
                redirect favorite
            }
            '*' { respond favorite, [status: OK] }
        }
    }

    def edit(Long id) {
        respond favoriteService.get(id)
    }

    def update(Favorite favorite) {
        if (favorite == null) {
            notFound()
            return
        }

        try {
            favoriteService.save(favorite)
        } catch (ValidationException e) {
            respond favorite.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'favorite.label', default: 'Favorite'), favorite.id])
                redirect favorite
            }
            '*'{ respond favorite, [status: OK] }
        }
    }

    @Secured (['ROLE_USER'])
    def delete() {
        def user = Usuario.findById(params.userID)
        def character = Character.findById(params.characterID)
        def toDelete = Favorite.findByUserAndCharacter(user,character)
        favoriteService.delete(toDelete)
        respond toDelete, [status: OK]
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'favorite.label', default: 'Favorite'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

package mcu

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }



        "/"(view:"/index")
        "/adminHome"(view:"/adminHome")
        "/usuario/register"(view:"/usuario/register")

        "/battle"(view:"/battle/")

        "500"(view:'/error')
        "404"(view:'/notFound')

        "/api/character"(controller: "character",action: "index")


        // ROUTES FOR USER REGISTRATION
        "/api/register"(controller: "register",action: "nuevoUsuario")

        "/api/user/modify"(controller: "usuario",action: "update")
        "/api/user/getID"(controller: "usuario",action: "findID")

        "/api/heroes"(controller:"hero",action: "index")
        "/api/villians"(controller:"villian",action: "index")

        "/api/battle/new"(controller:"battle",action: "save")
        "/api/battle/ended"(controller:"battle",action: "battlesEnded")

        "/api/fight/new"(controller:"fight",action: "fight")
        "/api/fight/"(controller:"fight",action: "index")
        "/api/fight/getFight"(controller:"fight",action: "getFightInfo")
        "/api/fight/myCurrentFights/"(controller:"fight",action: "myCurrentFights")
        "/api/fight/myEndedFights/"(controller:"fight",action: "myEndedFights")

        "/api/vote/"(controller:"vote",action: "save")
        "/api/vote/user/"(controller:"vote",action: "userVotes")

        "/api/vote/getVotes/"(controller:"vote",action: "getBattleVotes")

        "/api/favorites/me"(controller: "favorite", action: "myFavorites")
        "/api/favorites/new"(controller: "favorite", action: "save")
        "/api/favorites/delete"(controller: "favorite", action: "delete")
        "/api/user/changePass"(controller:"usuario",action: "changePassword")
    }
}



// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.mcu.Usuario'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.mcu.UsuarioRole'
grails.plugin.springsecurity.authority.className = 'com.mcu.Role'
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/redirect'
grails.plugin.springsecurity.securityConfigType = "InterceptUrlMap"
grails.plugin.springsecurity.interceptUrlMap  = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/adminHome.gsp',  access: ['ROLE_ADMIN']],
	[pattern: '/adminHome',      access: ['ROLE_ADMIN']],
	[pattern: '/redirect',       access: ['ROLE_ADMIN']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/usuario/register',access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']],
    [pattern: '/register/**', access: ['permitAll']],


		// RUTAS PARA EL DASHBOARD DEL ADMIN
	[pattern: '/villian/**',       access: ['ROLE_ADMIN']],
	[pattern: '/hero/**',       access: ['ROLE_ADMIN']],
	[pattern: '/usuario/**',       access: ['ROLE_ADMIN']],

	[pattern: '/api/character/**', access: ['ROLE_ADMIN']],
	//ROUTE FOR REGISTER A USER
	[pattern: '/api/register', access: ['ROLE_API']],
	[pattern: '/api/heroes', access: ['ROLE_USER']],
	[pattern: '/api/villians', access: ['ROLE_USER']],
	[pattern: '/api/user/modify', access: ['ROLE_API']],
	[pattern: '/api/user/getID', access: ['ROLE_API']],
	[pattern: '/api/battle/new', access: ['ROLE_USER']],
	[pattern: '/api/fight/new', access: ['ROLE_USER']],
    [pattern: '/api/fight/', access: ['ROLE_USER']],
    [pattern: '/api/fight/getFight', access: ['ROLE_USER']],
	[pattern: '/api/vote/', access: ['ROLE_USER']],
	[pattern: '/api/vote/user/', access: ['ROLE_USER']],
	[pattern: '/api/vote/getVotes/', access: ['ROLE_USER']],
	[pattern: '/api/battle/ended', access: ['ROLE_USER']],
	[pattern: '/api/fight/myCurrentFights/', access: ['ROLE_USER']],
	[pattern: '/api/fight/myEndedFights/', access: ['ROLE_USER']],
	[pattern: '/api/favorites/me', access: ['ROLE_USER']],
	[pattern: '/api/favorites/new', access: ['ROLE_USER']],
	[pattern: '/api/favorites/delete', access: ['ROLE_USER']],
	[pattern: '/api/user/changePass', access: ['ROLE_API']]

]


grails.plugin.springsecurity.filterChain.chainMap = [
		//unsecured
		[pattern: '/assets/**',      filters: 'none'],
		[pattern: '/**/js/**',       filters: 'none'],
		[pattern: '/**/css/**',      filters: 'none'],
		[pattern: '/**/images/**',   filters: 'none'],
		[pattern: '/**/favicon.ico', filters: 'none'],

		//Stateless chain for REST API
		[
				pattern: '/api/**',
				filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter'
		],

		//Traditional chain for form based login
		[
				pattern: '/**',
				filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'
		]
]

grails.plugin.springsecurity.rest.token.storage.useGorm = true
grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName = "com.mcu.RestAuthenticationtoken"
grails.plugin.springsecurity.rest.token.storage.gorm.tokenValuePropertyName = "token"
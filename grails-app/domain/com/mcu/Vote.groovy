package com.mcu

class Vote {

    Integer votes_character1
    Integer votes_character2

    static belongsTo = [battle:Battle, user:Usuario]

    static constraints = {
    }

    static mapping = {
        version false
    }
}

package com.mcu

class TypeCharacter {


    String name

    static hasMany = [costumes:Character]

    static constraints = {
        name nullable: false
    }

    static mapping = {
        version false
    }

    String toString(){
        name
    }
}

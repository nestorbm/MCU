package com.mcu

class RestAuthenticationtoken {

    String token
    String username

    Date dateCreated

    static mapping = {
        version false
    }

    static constraints = {
        dateCreated nullable: true
    }
}

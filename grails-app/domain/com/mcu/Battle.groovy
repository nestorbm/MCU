package com.mcu

import java.sql.Date

class Battle {

    Date deadline

    static belongsTo = [user:Usuario]
    static hasMany = [fight:Fight,votes:Vote]

    static mapping = {
        deadline  sqlType: 'date'
        version false
    }
    static constraints = {
    }
}

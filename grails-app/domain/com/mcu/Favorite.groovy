package com.mcu

class Favorite implements Serializable{
    Usuario user
    Character character

    static mapping = {

        //supposed to make a composite PK
        id composite:['user', 'character']
        version false
    }

    //static belongsTo = [user:Usuario,character:Character]

    static constraints = {
    }
}
